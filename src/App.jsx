import { Component } from 'react';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';
import { checkSession,  logout } from './api/auth';
import RegisterForm from './containers/RegisterForm';
import LoginForm from './containers/LoginForm';
import Home from './components/Home';
import Travels from './containers/Travels';
import Hotels from './containers/Hotels';
import Reviews from './components/Reviews';
import './App.scss';

class App extends Component {

  state ={
    error: '',
    hasUser: null,
  };

   componentDidMount() {
    this.checkUserSession();
  };

  async checkUserSession() {
    try {
      const data = await checkSession();
      delete data.password;
      this.setState({ user: data, hasUser: true })

    }catch(error) {
      this.setState({ error: error.message, hasUser: false });
    }
  };

  saveUser = user => {
    delete user.password;
    this.setState({ user, hasUser: true, error:'' });
  };

  logoutUser = async () => {
    try{
      await logout();

      this.setState({
        user: null,
        hasUser: false,
      });

    }catch(error){
      this.setState({ error: error.message, hasUser: false });
    }
  }
  
  render(){
    return (
      <Router>
{/*       {this.state.error && <p style={{color: 'red'}}>Ha ocurrido un error: {this.state.error}</p>} */}
        <div className="App">
        {this.state.hasUser && <button className="App__button__logout" onClick={this.logoutUser}>Logout</button>}
        <Switch>
          <Route exact path="/register" component={props => <RegisterForm {...props} saveUser={this.saveUser}/>}/>
          <Route exact path="/login" component={props => <LoginForm {...props} saveUser={this.saveUser}/>}/>
          <Route exact path="/travels" component={props => <Travels {...props} saveUser={this.saveUser}/>}/>
          <Route exact path="/hotels" component={props => <Hotels {...props} saveUser={this.saveUser}/>}/>
          <Route exact path="/reviews" component={props => <Reviews {...props} saveUser={this.saveUser}/>}/>
          <Route exact path="/" component={props => <Home {...props} user={this.state.user}/>}/>
        </Switch>
        </div>
     </Router>
    )
  }
}

export default App;
