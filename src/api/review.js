const reviewUrl = 'http://localhost:4000/reviews/';

export const reviews = async (data) => {
  const request = await fetch(reviewUrl, {
    method: "POST",
    headers: {
      "Accept": "application/json",
      "Content-Type": "application/json",
      "Access-Control-Allow-Origin": "*",
    },
    credentials: 'include',
    body: JSON.stringify(data),
  })
  const response = await request.json();
  if(!request.ok) {
    throw new Error(response.message);
  }
  return response;
}