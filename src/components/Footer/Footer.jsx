import './Footer.scss';

const { Component } = require("react");

class Footer extends Component{
    render(){
        return(

            <footer className="footer">
                <nav className="footer__navbar">
                    <div className="footer__navbar__left">
                        <p>Agencia de viajes desde 1989 ©</p> 
                    </div>
                    <div className="footer__navbar__right">
                        <div className="footer__navbar__right__insta">
                            <img src="/icons/insta.png" alt="instagram"/>
                            <p>Intagram</p>
                        </div>
                        <div className="footer__navbar__right__facebook">
                            <img src="/icons/facebook.png" alt="facebook"/>
                            <p>Facebook</p>
                        </div>
                        <div className="footer__navbar__right__twitter">
                            <img src="/icons/twitter.png" alt="twitter"/>
                            <p>Twitter</p>
                        </div>
                        <div className="footer__navbar__right__about">
                            <p>¿Quieres saber más?</p>
                            <hr/>
                            <p>Contáctanos</p>
                        </div>
                    </div>
                </nav>
            </footer>
        )
    }
}

export default Footer;