import { Component } from  'react';
import { Link } from 'react-router-dom';
import './Header.scss';

class Header extends Component{

    render(){
        return(
            <header className="header">
                <nav className="header__navbar">
                    <div className="header__navbar__left">
                        <span>
                            <img className="header__navbar__left__img" src="/images/logo.png" alt="logo agencia"/>
                        </span>
                    </div>
                    <div className="header__navbar__right">
                        <Link className="header__navbar__right__link" to="/">Home</Link>
                        <Link className="header__navbar__right__link" to="/travels">Viajes</Link>
                        <Link className="header__navbar__right__link" to="/hotels">Hoteles</Link>
                        <Link className="header__navbar__right__link" to="/reviews">Reseñas</Link>
                    </div>
                </nav>
            </header>
        )
    }
}

export default Header;