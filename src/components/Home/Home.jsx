import { Component } from 'react';
import { Link } from 'react-router-dom';
import Footer from '../Footer/Footer.jsx';
import Header from '../Header/Header.jsx';
import './Home.scss';

class Home extends Component{
    render(){
        return(
            <>
                <Header/>
                <div className="home">
                    <div className="home__box">
                        <div className="home__box1">
                            <div className="home__box1__title">
                               <p>Los mejores viajes y hoteles</p> 
                            </div>
                            <div className="home__box1__title2">
                                <p>que puedas encontrar</p>
                            </div>
                        </div>
                        <div className="home__box2">
                            <div className="home__box2__register">
                            {!this.props.user &&<Link className="home__box2__register__link" to="/register">¡Regístrate!</Link>}
                            </div>
                            <div className="home__box2__login">
                            {!this.props.user &&<Link className="home__box2__login__link" to="/login">Inicio de Sesión</Link>}
                            </div>
                        </div>
                    </div>
                </div>
                <Footer/>
            </>
        )
    }
}

export default Home;