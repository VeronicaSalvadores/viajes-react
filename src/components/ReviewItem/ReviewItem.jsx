import { Component } from "react";
import PropTypes from 'prop-types';

class ReviewItem extends Component{
    
    render(){
        const { name, title, description } = this.props.review;

        return(
            <div>
                <h3>{name}</h3>
                <p>Asunto: {title}</p>
                <p>Mensaje: {description}</p>
            </div>
        )
    }
}

ReviewItem.propTypes = {
    review: PropTypes.shape({
      name: PropTypes.string,
      title: PropTypes.string,
      description: PropTypes.string,
    }),
}

export default ReviewItem;