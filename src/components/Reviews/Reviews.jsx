import { Component, Fragment } from "react";
import ReviewForm from "../../containers/ReviewForm/ReviewForm";
import ReviewItem from "../ReviewItem/ReviewItem";
import Footer from '../../components/Footer';
import Header from '../../components/Header';
import './Reviews.scss';


class  Reviews extends Component{

    state = {
        reviewsList: [],
        reviews: [],
      };

    addReview = newReview => {
        this.setState({
        reviewsList: [...this.state.reviewsList, newReview]});
    }

    componentDidMount() {
        this.getReviews();
    }

    getReviews(){
        fetch("http://localhost:4000/reviews/")
        .then(res => res.json())
        .then(response => this.setState ({ reviews: response }));
    }

    render(){
        return(
            <Fragment>
                <Header/>
                <div className="reviews">
                    <h1 className="reviews__title">Reseñas</h1>
                    <div className="reviews__box">
                        {this.state.reviews.length ? (
                        this.state.reviews.map((reviewOld) => (
                          <div className="reviews__box__list" key={(reviewOld)}>
                            <h3>{reviewOld.name}</h3>
                            <p>Asunto: {reviewOld.title}</p>
                            <p>Descripción: {reviewOld.description}</p>
                          </div>
                        ))
                        ) : (
                        <h3>¡No hay reseñas cargadas!</h3>
                        )}
                        {this.state.reviewsList.map((newReview) => (
                            <div className="reviews__box__list">
                                <ReviewItem key={`${newReview.name} ${newReview.title} ${newReview.description}`} review={newReview} />
                            </div>
                        ))}                        
                    </div>           
                </div> 
                <ReviewForm addReview={this.addReview}/>
                <Footer/>
            </Fragment>
        )
    }
}

export default Reviews;