import { Component } from "react";
import Footer from '../../components/Footer';
import Header from '../../components/Header';
import './Hotels.scss'

class Hotels extends Component{
    
    state = {
        hotelsList: [],
    }

    componentDidMount() {
        this.getHotels();
    }

    async getHotels(){
        await fetch("http://localhost:4000/hotels/")
        .then(res => res.json())
        .then(response => this.setState ({ hotelsList: response }));
    }

    render(){
        return (
            <div>
                <Header />
                <div className="hotels">
                    <h1 className="hotels__title">¡Elige tu hotel!</h1>
                    <div className="hotels__box">
                        {this.state.hotelsList.length ? (
                        this.state.hotelsList.map((hotels) => (
                          <div className="hotels__box__list" key={JSON.stringify(hotels)}>
                            <img className="hotels__box__list__image" src={hotels.image} alt={hotels.name}/>
                            <h3 className="hotels__box__list__name">{hotels.name}</h3>
                            <p>Ubicación: {hotels.location}</p>
                            <p>Estrellas: {hotels.stars}</p>
                            <h2>{hotels.price}€</h2>
                          </div>
                        ))
                        ) : (
                        <h3>¡No hay hoteles cargados!</h3>
                        )}
                    </div>
                </div>
                <Footer />
            </div>
        )
    }
}

export default Hotels;