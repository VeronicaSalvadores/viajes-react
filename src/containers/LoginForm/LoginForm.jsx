import { Component } from 'react';
import { Link } from 'react-router-dom';
import { login } from '../../api/auth';
import './LoginForm.scss';

const INITIAL_STATE = {
    email: '',
    password: '',
    error: '',
}

class LoginForm extends Component {

    state = INITIAL_STATE;

    handleSubmitForm = async ev => {

        ev.preventDefault();
        const { history, saveUser } = this.props;

        try {
            const data = await login(this.state);
            saveUser(data);
            this.setState(INITIAL_STATE);
            history.push('/');
            
        }catch(error) {
            this.setState({error: error.message});
        }
    };

    handleChangeInput = (ev) => {
        const { name, value } = ev.target;
        this.setState({ [name]: value });
    };

    render(){
        return(
            <div className="login">
                <Link className="register__home" to="/">Home</Link>
                <form className="login__box__form" onSubmit={this.handleSubmitForm}>
                <h1 className="login__box__form__title">Inicio de Sesión</h1>
                <hr/>
                    <label className="login__box__form__email" htmlFor="email">
                        <p className="login__box__form__pass__text">Email:</p>
                        <input type="text" name="email" value={this.state.email} onChange={this.handleChangeInput}/>
                    </label>
                    
                    <label className="login__box__form__pass" htmlFor="password">
                        <p className="login__box__form__pass__text">Contraseña:</p>
                        <input type="password" name="password" value={this.state.password} onChange={this.handleChangeInput}/>
                    </label>

                    {this.state.error && <p style={{color: 'red'}}>Ha ocurrido un error: {this.state.error}</p>}

                    <div>
                        <button className="login__box__form__button" type="submit">Entrar</button>
                    </div>
                </form>
            </div>
    )}
}
    

export default LoginForm;