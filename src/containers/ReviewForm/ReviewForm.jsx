import { Component } from "react";
import { reviews } from '../../api/review';
import './ReviewForm.scss';


const INITIAL_FORM = {
    name:'',
    title:'',
    description:'',
}

class ReviewForm extends Component{

    state = INITIAL_FORM;

    handleSubmitForm = async ev => {
        ev.preventDefault();

        try{
        await reviews(this.state);
        this.props.addReview(this.state);
        this.setState(INITIAL_FORM);

        } catch(error){
            this.setState({error: error.message});            
        }
    }

    handleChangeInput = ev =>{
        const { name, value} = ev.target;
        this.setState({ [name]: value});
    }

    render(){
        return(
            <div className="reviewForm">
                <div className="reviewForm__form">
                    <h1 className="reviewForm__title">Escribe tu reseña</h1>
                    <hr className="reviewForm__hr"/>
                <div className="reviewForm__box">
                    <form onSubmit={this.handleSubmitForm}>
                        <label htmlFor="name">
                            <p>Nombre </p>
                            <input type="text" name="name" value={this.state.name} onChange={this.handleChangeInput} />
                        </label>

                        <label htmlFor="title">
                            <p>Asunto </p>
                            <input type="text" name="title" value={this.state.title} onChange={this.handleChangeInput} />
                        </label>

                        <label htmlFor="description">
                            <p>Mensaje </p>
                            <input type="text" name="description" value={this.state.description} onChange={this.handleChangeInput} />
                        </label>
                        <div className="reviewForm__box__button">
                            <button className="reviewForm__box__button__b" type="submit">Añadir reseña</button>                            
                        </div>
                    </form>
                </div>
                    
                </div>
            </div>
        )
    }
}

export default ReviewForm;