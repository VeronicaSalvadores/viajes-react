import { Component } from 'react';
import { Link } from 'react-router-dom';
import Footer from '../../components/Footer';
import Header from '../../components/Header';

import './Travels.scss'

class Travels extends Component {

    state = {
        travelsList: [],
    }

    componentDidMount() {
        this.getTravels();
    }

    async getTravels(){
        
        await fetch("http://localhost:4000/travels/")
        .then(res => res.json())
        .then(response => this.setState ({ travelsList: response }));
    }

    render(){
        return (
            <div>
                <Header />
                <div className="travels">
                    <h1 className="travels__title">¡Elige tu viaje!</h1>
                    <div className="travels__box">
                        {this.state.travelsList.length ? (
                        this.state.travelsList.map((travels) => (
                          <div className="travels__box__list" key={JSON.stringify(travels)}>
                            <img className="travels__box__list__image" src={travels.image} alt={travels.name}/>
                            <h2 className="travels__box__list__name">{travels.name}</h2>
                            <p>Duración: {travels.time}</p>
                            <h3>{travels.price}€</h3>
                            <Link to={`/travels/${travels._id}`}>Ver más</Link>
                          </div>
                        ))
                        ) : (
                        <h3>¡No hay viajes cargados!</h3>
                        )}
                    </div>
                </div>
                <Footer />
            </div>
        )
    }
}

export default Travels;